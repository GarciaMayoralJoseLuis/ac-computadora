# ac-computadora

___Mapas conceptuales___



## Von-Neumann vs Hardvard

[//]: # (Encabezado nivel 1   Es una alternativa para poner titulo
        ==================          )

#### Sintaxis Markdown
```plantuml
@startmindmap
*[#FFBBCC] Von-Neumann vs Hardvard 
	*[#lightblue] Von-Neumann
		*[#lightblue] Historia.
			*[#lightblue] Fue creado por el cientifico János Von Neumann, dicho cientifico:\n*Nació en Budapest Hungría un 28 de diciembre de 1903 y falleció un 8 de febrero de 1957. \n*Participó activamente en la creación del primer ordenador. \n*Propuso al Bit como unidad de medida de la memoria
		*[#lightblue] Modelo Newuman. \nDicha arquitectura aún es utilizada.
			*[#lightblue] Principales características
				*[#lightblue] *Hay un único espacio de memoria de lectura y escritura, que contiene las instrucciones y los datos necesarios.\n*El contenido de la memoria es accesible por posición, independientemente de que se acceda a datos o a instrucciones.
			*[#lightblue] Dicha arquitectura se divide en 4 partes visibles.
				*[#lightblue] Procesador
					*[#lightblue] Conocido también como CPU, funge como cerebro del PC. \nSe dedica a ejecutar aplicaciones y el sistema operativo.
				*[#lightblue] Dispositivo de almacenamiento
					*[#lightblue] También llamado memoria, \nconsta de dos tipos de almacenamiento.
						*[#lightblue] Almacenamiento a largo plazo.
							*[#lightblue] Disco duro: Este almacena de forma masica informacion\n a largo plazo. Se organiza mediante pistas, sectores y sectores de pistas.
							*[#lightblue] Memorias ROM:La memoria ROM se llama así por las siglas\n en inglés Read Only Memory, o memoria de solo lectura.\n El ejemplo más famoso es el BIOS.
						*[#lightblue] Almacenamiento a corto plazo.
							*[#lightblue] Memoria RAM: La memoria RAM se llama así por las siglas en \ninglés Random Access Memory, o memoria de acceso aleatorio. \nSu nombre es este precisamente porque su contenido cambia \nconstantemente dependiendo de lo que necesite el PC en cada\n momento para que el procesador tenga un acceso a ellos de\n manera inmediata, en tiempo real de hecho.
				*[#lightblue] Dispositivo de entrada y salida(E/S)
					*[#lightblue] Dispositivo de entrada: Permiten el ingreso de información al\n computador mediante diferentes métodos. Algunos ejemplos\n serian, teclado y raton.
					*[#lightblue] Dispositivo de salida: Permiten la saida de información del \n computador. Algunos ejemplos serian, monitor, impresora.
				*[#lightblue] BUSES
					*[#lightblue] BUS DE DATOS: Un bus de datos es un dispositivo mediante el cual\n al interior de una computadora se transportan datos e información relevante.
					*[#lightblue] BUS DE DIRECCIONES: Este bus se utiliza para direccionar las zonas de memoria \ny los dispositivos.
					*[#lightblue] BUS DE CONTROL: A través de este bus circulas las señales de control \nde todo el sistema, dado que es de entrada y salida, esto debido a que \nla CPU envía y recibe señales.
	* Modelo Hardvard
		* Historia
			* Tuvo origen en el año de 1944 y toma su nombre de la computadora Hardvard Mark\n creada por el científico Howard H. Aiken.			
		* ¿Qué es?
			* Es una arquitectura de computadora con pistas de almacenamiento y de señal físicamente \nseparadas para las instrucciones y para los datos.
		* Usos modernos.
			* *Procesadores de señal digital (DSPs) \n*Microcontroladores 
		* Principales caracteristicas
			* Constaba de dos memorias, una memoria que almacenaba instrucciones y otra memoria \nque almacenaba datos.
			* Utilizaba dos memorias conectadas mediante buses independientes conectadas a la unidad \nde procesamiento central, una memoria almacenaba los datos y otra las instrucciones.
			* El CPU podía acceder de forma simultánea a ambas memorias.
			*  Otra de sus caracteristicas es el contar dos buses de diferente tamaño y distintos \ncontenidos en la misma pocicion de memoria.
@endmindmap
@enduml
```
[//]: # (Importante: borrar el 1 al final de la liga para poder acceder al link y poder observar como se realizó el mapa)
## Supercomputadoras en México :computer:


#### Sintaxis Markdown
```plantuml
@startmindmap
*[#FFBBCC] Supercomputadoras en México 
	*  ¿Para qué se usan las supercomputadoras?
		*[#lightblue] En las universidades se utilizan para realizar todo tipo de proyectos de investigación.
	*  Historia de la computación en México
		*[#lightgreen] 1958
			*[#lightblue] Hace mas de 51 años inicia la era de la computación en México.\nEsto sucede a través de la adquisición de una computadora IBM por parte de la Universidad \nNacional Autónoma de México en 1958.
			*[#lightblue] La adquisición fue una **IBM 650**.
				*[#FFBBCC] Características: \n*Contaba con una memoria total de 2KB. \n*Al igual que las primeras computadoras, funcionaba con bulbos. \n*Su capacidad de procesamiento era en total de mil operaciones \naritmeticas por segundo.
		*[#lightgreen] Mediados de los 80's
			*[#lightblue] Las computadoras empezaron a ser de uso común en oficinas y hogares.
		*[#lightgreen] 1991
			*[#lightblue] La UNAM adquiere la primera supercomputadora de América Latina.
			*[#lightblue] La adquisición fue una **CRAY 432**.
				*[#FFBBCC] Características: \n*Dicha supercomputadora equivalía a 2 mil computadoras de oficina. \n*Estuvo al servicio de la UNAM por al rededor de 10 años.
		*[#lightgreen] 2007
			*[#lightblue] Se pone en funcionamiento \nla supercomputadora llamada **KanBalam**.
				*[#FFBBCC] Características: \n*Equivalía a mas de 1300 computadoras de escritorio. \n*Podía relizar 7 millones de operaciones matemáticas por segundo.
		*[#lightgreen] 2012
			*[#lightblue] Creación **Laboratorio Nacional de Cómputo de Alto Desempeño**\n(LANCAD) por parte de la UNAM, UAM Y CINVESTAV.
				*[#FFBBCC] De aquí deriva el **Xiuhcoatl**: la supercomputadora del Politécnico que\n es única en el mundo. Es la primer supercomputadora híbrida del mundo.
				*[#FFBBCC] La **Xiuhcoatl** es la segunda supercomputadora más potente de América Latina\n con un rendimiento de 252 Teraflops.
			*[#lightblue] La UNAM pone en funcionamiento \na la quienta generación de supercomputadoras **Miztli**. 
				*[#FFBBCC] Características: \n*Cuenta con una velocidad de 228 Teraflops.
		*[#lightgreen] 2013
			*[#lightblue] En el año 2013 nació una iniciativa por parte de un conjunto de investigadores\nde la Benemérita Universidad Autónoma de Puebla (BUAP), de aquí surgió la \nsupercomputadora de la BUAP.
				*[#FFBBCC] Características: \n*La quinta supercomputadora más rapida de América Latina.\n*Tiene una capacidad de almacenamiento equivalente a casi \n5 mil computadoras portatiles.\n*Se usa principalmente para simulaciones en el sector \nautomotriz y aeronáutica.

@endmindmap
```

[//]: # (Importante: borrar el 1 al final de la liga para poder acceder al link y poder observar como se realizó el mapa)

[PlantUML 1]: http://www.plantuml.com/plantuml/umla/ZLVDRXit4BxdAGRefKrKzQSfsleKMr7W5B5bH7OlTGyZSYopuN8s95U1ypOvvk13q5klUh7EKBkBNKWrV12m16U6yttp3JbwaJB6NBbWAwoVFFlpgrUlJavcix_W3mxVdbDJOGYmIN2AqMxa1q-UYP5twzkyyWsDx6Hhl7VgKkReSAexuwrN3O69X9QXvWZamJWAsTqum_3xzbFWfA6X3Jq1wymj3uoEhiFJSpHk-mzGW9F6OaqfmsaJrd5xZz04-F5NiAIEZgfL95tyyDltFy4TtA3tfBvgLgnkI2mYxsn-_cKA4lu2OnRdMipGPBV1IWudFSvZILuZ177nK4TNKGIEbe9WYZl_o7MJ6D33YSjWk68vprcqUan5LZxrowCiAYLgZy4pbkWiB7nG6ZJgI-410UFxncKokOcosFuRW18qsNdt4UqXlY-Y2m85lTWPZ8A7XAOa-30Lwx7vqrEyKsuaiXQ4KYrWbPqkqv9tU_wTx9lez7i2xxMmB4MYeCWIk91oR8ou1qfYwpc1nSm90XbA61sdgR0rzyMFX20UiwBOq8YjMtbIkSWlkKB-HC9RgYbO5GQL0gcpPf6eU5a4R0zam74w1uWwm6zI4MsSBQL_3ZN6J0asJlDfO-onpGhDSfJDlTqP1pQACcErSjlFOIUDsSNL16v4lxHR6ueYIYxwkfXfxPUAJVIiYEXlCat625XxPx2dLuoJIq850jSKKNJB-oagpgGiwhOIXhv2Gm4hvP4FPNtP9UizLWAjgyv44ZCih6BfDawGdKJU3wfPx8Sz7ja0WiUuPgWzVkmzzbCtadGJ-GZc0hK_fpGoHu5UOLAQND1b8USwm3Ws29iSrnYaPKepecgdTW8aJI19xJaMHljFsst_JrvdEq8Il5sS7RtfvQjB2RBZ3J1jxuk6jJUIMtjCaXe5oNNjjv_5cT328lYxNRox2V2mwH9xxZflAfvphrAeQYzMsqy9Rh3Yf1shIZXvlLW-adR3CJzC-ra7v_Zi28RmZX-6f-2kGmyFW-KAZhMVqs68g7jwRHBgyy55-Wq4rbky84jQgKZ6fVPEbXDBopU3EyIeOB5NPtyzzFT5Uyymg6kvZ9nibsxJd0rQ588hLZVMjaUrL8Ewxs-GmzRwslHPoxulybTV31HLXaLV6ISQDpif5qCaz7h-BPdRntIe917HeWeIlRFuzVoxvJVgzZY_8xYWM3b9Lm58DI8btUzxfBnbwA_BRLGtkQ3h-yAw6ufANnAPVSviEKr5HEicSEe4g49E5CLX0fcCbmYIRSJCOVg8H1NMA4-FaD1PtCjIRyHHdlkfz8bK75pcE05NrGevuY6kJwwMy-M177K3NixXvV7bOda4Lm5MZTQsVL0eQUNj04TFc4uqJQ4JbUuiwN1KpoRW8FFokkI8GKB4ZA4BF0OSoTD6mquVol7rs_bizdfnFb-szwBccgYR2tOAjYvIUVrmrwCVvIiD7_RhKDxe8QhqqB6pnVdbsyMR8pXM71ljSLLTTxvnqJHwaFuIRR-WfsAXZHfP2oaB5TJWwPg0LV6KRak7n3_H0rGrA-rqL4i9v54VKJ7URE_LLc-85VMdAefsekhdMGNMpwa59LmsKaTPhYd0hbjm-wMT39y_bx2PAuJKNqYouKSbxOF36SPtAbGTT74qw6xlowHxoX_KxbG4xDxHhaqEpcfkCAh1h5neg_x1RKkN9YgkRgMSz2_Z2lVcCCuH1qKVMwiCkxB3rKu_AnmTfWKa8uWnzQhNYbCQpsGZGmIFtGoiXUzkqlOPCgNEVeHZa5K_E_RvxmsORSfpNvHGirKbb36G8lJZGN9Lu_FsKo3P1AnMsEFg8i12ugj7QlQgqBwkgc2CIagBsmxkIHtkL3UIgLKtWPS4SlyyfVP_HELIfT8mJW4C6d7wv8LGsFup-my0

[PlantUML 2]: http://www.plantuml.com/plantuml/uml/bLPFRnl73xtxKn3u7Nw9R0YIN3TEBeqaAqWGsp1gEsXG-K3jql8KipER-IFK_ZOv-k13aLil1RfVhEHe84jP6NLFmkuE7nyV7pbwwmEwK2bJLbZlxFxwltVlHgFn-0hEOqski5KT0vRMeGSoSDBS_wuA2pkxk_3tNsVe4Bx4vXuyGVHeGFCn_ttWJtoQWRMQtuIPZdG5a-N9QDI2d5SbbkGJHb1QtJ5EBSYEK9uS15jQ2AgsK1BKpjvI4QoN1sKMv8EQOw6QRwOhjDuh7wnJA9yrGYQIlgzLi4PfxeZC5VHV7npAsqscxx4Wg30bE-W3DdzmOcLKeL3GoVrBdkxKJ9WFYr8G7qG83XVDlSzHM7w9ogiK9Q-YGLYJ3ZwCJg2sJlG8b6Ck7rM3gJdbbDQWXc4C368h8RJg4vbKLxTTsF5toQ_ZCdkd8pb_FEXrEikeDKkCqM4Ho3KFh7c1_Wtdxuoj2JWJsYO1L5Gb-OCDg8NBuEEeAmU76jGyecR7K6f_xLH5Ognri-mn4vDAofYpg6VM9uJp20NMM4ZfsGe5UQmKcM1JByWyvgsK1ijMJ0AHPm1qAbIKg2TLFSsZAMstROOJAXNpMRQ9Vmvx__VRDDpa3bJLn8PbriZWBdNKMpdJ_9bSOQzPRiCdR-76pj6Hxsvpukl-rePTdWvFbbqZbwoG5MoDd2GULiszur8vC73A_sI1yS_3p_33_k2v7ZXInSqM5iJW2zJD0oU2WJHaKovccGL9xUL1YGiBg4Mx1R-tIVvSjpICFpagYQFbIx-Nvd6RX8DUxrMxu7DY54D0PcMox9sfqLluQuqLbYB8HpGZvAVd2Z9PhpolZ_v-hzUgdtpXbEmhcoGuiwN4E5eklbUYcKxkBMd3p1K6gfglp_0oQz4VjBKOEyhBgjCvnXdpIInWjKyuuRZv9cnjifFctoFoOd4MlTEPcXV7mzFnyEXbUqDnl_RWafls6SOVJZzDpY-6drh27H4WNnuF7EHOB23DL7vHyQQm67IdymRqLcThEBDQXUQ-CAgmiasc1iW3ZvZOdOmWLKaCcFZ7CMbZtJGFCwTA0SqHBPB7k4aAI12psbleSK-a7u5Ce2rJE3LvJu8ZKwhiFbcI1mEu88VNsjQ--_JqF-bVP5giXJmYpCamrkfMQTt8NDE9kWjQiP_XULi-2l1goIz8szKQ7WmEDxct3RZVhcWYVKe3d0w0INm5FT-lWKsnQQsOrFijSiKRbp_NH7vgbkORaQ4aUjYyAZVknxD8CpxxOdGvF7kvns-o5NrqSs6XaOjkDJSdu9ZdRO7Zr14JdlA9mrgLMvSr1ryeCcaVRzvvg2iiQDNul6AJtUIaLvpueBrgMSV0q9gyO9ypi4zpmIwg6JB5fpzTNbLHvosJPybJ4QmJZyLWAnkSkeDRG7BMD5-ZbDlTsNdBVix_8ly1

[GarciaMayoralJoseLuis  @ GitLab](https://gitlab.com/GarciaMayoralJoseLuis)
